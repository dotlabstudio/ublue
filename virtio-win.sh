#!/bin/bash
#add virtio repo to a fedora system
sudo wget https://fedorapeople.org/groups/virt/virtio-win/virtio-win.repo \
  -O /etc/yum.repos.d/virtio-win.repo
#source: https://github.com/virtio-win/virtio-win-pkg-scripts/blob/master/README.md
#install virtio-win on ublue sytem or fedora kiniote or silverblue system
rpm-ostree install virtio-win
