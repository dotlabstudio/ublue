#!/usr/bin/env bash
#
#update system
#
rpm-ostree update
#
#install flathub remote as a user not system wide
#flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak remote-add --user --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo


#
#vim, distrobox, wget, curl, git, toolbox, htop and tailscale are installed by default
#
#add virtio-win repo
sudo wget https://fedorapeople.org/groups/virt/virtio-win/virtio-win.repo \
  -O /etc/yum.repos.d/virtio-win.repo
#
#install layered packages
rpm-ostree install bat bridge-utils edk2-ovmf exa fira-code-fonts fish fontawesome5-fonts-all guestfs-tools jetbrains-mono-fonts-all libdbusmenu-devel libguestfs libvirt libvirt-daemon-config-network libvirt-daemon-kvm libvirt-daemon-lxc lsd neofetch procs python3-libguestfs qemu-kvm ripgrep sd tailscale tealdeer util-linux-user virt-install virt-manager virt-top virt-viewer virtio-win xrdp yt-dlp



#
# forgot that fedora silverblue and kiniote can not add to groups for some reason
#for post install of virt manager instructions
#https://fedoramagazine.org/full-virtualization-system-on-fedora-workstation-30/
#https://en.opensuse.org/Portal:MicroOS/Virtualization
#https://discovery.endeavouros.com/applications/how-to-install-virt-manager-complete-edition/2021/09/ - not really needed
#but good for reference
