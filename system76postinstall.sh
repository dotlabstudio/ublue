#!/bin/bash
#
#post install script after installing the system76 drivers on the rpm-ostree
#after reboot
#enable the system76 driver services
sudo systemctl enable com.system76.PowerDaemon.service system76-power-wake system76-firmware-daemon --now
systemctl enable --user com.system76.FirmwareManager.Notify.timer
system76-power graphics integrated
#
#Fedora 37 on: requires the power-profiles-daemon to be masked
#
sudo systemctl mask power-profiles-daemon
sudo systemctl daemon-reload 
#
#System76 Power in Fedora from official system76 website
#Use these commands to install the System76 Power package and enable the service:
#
#sudo systemctl enable com.system76.PowerDaemon.service system76-power-wake
#sudo systemctl start com.system76.PowerDaemon.service
#Some users may find that the com.system76.PowerDaemon.service service does not start automatically on boot, even though it's enabled. #To remedy this, you may need to mask the power-profiles-daemon.service:
#
#sudo systemctl mask power-profiles-daemon.service

echo "please reboot the system"
#reboot again
