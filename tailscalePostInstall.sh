#!/bin/bash
#
#post install of tailscale
#Use systemctl to enable and start the service:
sudo systemctl enable --now tailscaled
#
#Connect your machine to your Tailscale network and authenticate in your browser:
sudo tailscale up
