#!/bin/bash
#
#virt manager post install scripts for ublue fedora silverblue and kiniote
#
#Set the domain socket group ownership to libvirt & Adjust the UNIX socket permissions for the R/W socket
#sudo vim /etc/libvirt/libvirtd.conf 
#and uncomment the following lines
#unix_sock_group = "libvirt"
#unix_sock_rw_perms = "0770"
#
#start and enable the libvirtd service
sudo systemctl start libvirtd
sudo systemctl enable libvirtd
#
#for ublue silverblue and kiniote groups
#sudo vim /etc/group
#add these lines at the end
#libvirt:x:964:
#
#check the /usr/lib/group file first for the above
#
#add user to group
# usermod -aG libvirt username
sudo usermod -a -G libvirt $(whoami)
#
#After installing, reboot into the new snapshot, and enable daemons:
#Define the default (masqueraded to host) network:
#sudo virsh net-define /etc/libvirt/qemu/networks/default.xml
#sudo virsh net-autostart default
#sudo virsh net-start default
#above is not needed for fedora - it relates to opensuse systems
#
#Optionally, to have machines state automatically saved and restored across reboots:
sudo systemctl enable --now libvirt-guests
#
echo "virt manager is setup now"
