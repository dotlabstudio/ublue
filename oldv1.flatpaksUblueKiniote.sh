#!/bin/bash

# install flatpaks from flathub
#https://flathub.org/home
#
flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
#  the above one installs flathub repos as a user

flatpak install --user --noninteractive flathub com.bitwarden.desktop

flatpak install --user --noninteractive flathub org.gnome.Boxes

flatpak install --user --noninteractive flathub com.brave.Browser

flatpak install --user --noninteractive flathub io.github.cudatext.CudaText-Qt5

flatpak install --user --noninteractive flathub org.kde.digikam

flatpak install --user --noninteractive flathub org.filezillaproject.Filezilla

flatpak install --user --noninteractive flathub io.kopia.KopiaUI

flatpak install --user --noninteractive flathub org.libreoffice.LibreOffice

flatpak install --user --noninteractive flathub net.sapples.LiveCaptions

flatpak install --user --noninteractive flathub org.kde.marble

flatpak install --user --noninteractive flathub org.getmonero.Monero

#flatpak install --user --noninteractive flathub org.mozilla.firefox

flatpak install --user --noninteractive flathub md.obsidian.Obsidian

flatpak install --user --noninteractive flathub org.onlyoffice.desktopeditors

flatpak install --user --noninteractive flathub com.nextcloud.desktopclient.nextcloud

flatpak install --user --noninteractive flathub io.github.peazip.PeaZip

flatpak install --user --noninteractive flathub org.standardnotes.standardnotes

flatpak install --user --noninteractive flathub org.strawberrymusicplayer.strawberry

flatpak install --user --noninteractive flathub com.github.micahflee.torbrowser-launcher

flatpak install --user --noninteractive flathub org.videolan.VLC

flatpak install --user --noninteractive flathub com.borgbase.Vorta

echo "Finished installing my flatpaks for ublue kiniote list"
