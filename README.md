# ublue Kinoite Project

This is my public repo for ublue kiniote.  As 2023-07-03, thanks to red hat, I will stop using fedora soon - probaby
move to either nixos or opensuse kalpa or debian on my main laptop.

See the main home page of Universal Blue - https://ublue.it/

For existing ublue or fedora kiniote or fedora silverblue installs, you can rebase with these images (don't forget to pin last working image first with sudo admin pin 0):

kinoite-main:

A base kinoite-main image with batteries included

```
rpm-ostree rebase ostree-unverified-registry:ghcr.io/ublue-os/kinoite-main:latest
```

kinoite-nvidia:

ublue-os kinoite with Nvidia drivers added

```
rpm-ostree rebase ostree-unverified-registry:ghcr.io/ublue-os/kinoite-nvidia:latest
```

ucore:

An OCI base image of Fedora CoreOS with batteries included

```
rpm-ostree rebase ostree-unverified-registry:ghcr.io/ublue-os/ucore:latest
```


For my purpose:
I setup my kinoite systems to like a nobara/ubuntu studio setup with most of apps in those distributions as flatpaks:

see nobara homepage at https://nobaraproject.org/

see ubuntu studio hompage at https://ubuntustudio.org/

For my personal perferences; I like ubuntu studio look and feel with top panel with global menu, 9 virtual desktops pager, 2 activities pager and netspeed widget. I just don't care for ubuntu and their snaps.

# Setup fish as the default shell on Kinoite

```
rpm-ostree install fish util-linux-user
```

reboot

then change shell normally with:

```
chsh -s /usr/bin/fish 
```

# Setup Tailscale on Kinoite

Add tailscale repo

```
sudo wget https://pkgs.tailscale.com/stable/fedora/tailscale.repo \
  -O /etc/yum.repos.d/tailscale.repo
```

Install tailscale

```
rpm-ostree install tailscale
```

reboot

Start tailscale services

```
sudo systemctl enable --now tailscaled
```

Connect new machine to the Tailscale network and authenticate in a browser:
```
sudo tailscale up
```

# Setup virt manager in Kinoite

```
rpm-ostree install bridge-utils edk2-ovmf guestfs-tools libdbusmenu-devel libguestfs libvirt libvirt-daemon-config-network libvirt-daemon-kvm libvirt-daemon-lxc python3-libguestfs qemu-kvm virt-install virt-manager virt-top virt-viewer xrdp
```

These were basically taken from fedora dnf groupinfo virtualization with 
bridge-utils edk2-ovmf libdbusmenu-devel libguestfs libvirt xrdp

```
Group: Virtualization
 Description: These packages provide a graphical virtualization environment.
 Mandatory Packages:
   virt-install
 Default Packages:
   libvirt-daemon-config-network
   libvirt-daemon-kvm
   qemu-kvm
   virt-manager
   virt-viewer
 Optional Packages:
   guestfs-tools
   python3-libguestfs
   virt-top
```


reboot

check the /usr/lib/group for these groups:

libvirt
kvm
qemu

with
```
sudo cat /usr/lib/group
```

copy those lines into /etc/group with:
```
sudo vim /etc/group
```

for example, append these line into /etc/group:
```
libvirt:x:964:
kvm:x:36:qemu
qemu:x:107:
```

Edit the /etc/libvirt/libvirtd.conf 

```
sudo vim /etc/libvirt/libvirtd.conf 
```

Set the domain socket group ownership to libvirt & Adjust the UNIX socket permissions for the R/W socket
and uncomment the following lines:

```
unix_sock_group = "libvirt"

unix_sock_rw_perms = "0770"
```

Start and enable the libvirtd service

```
sudo systemctl start libvirtd
sudo systemctl enable libvirtd

sudo systemctl enable --now libvirt-guests
```
Add user to group with usermod -aG libvirt username:
```
sudo usermod -a -G libvirt $(whoami)
```

reboot the system

# Distrobox

For other command line programs; use distrobox.

To create a fedora distrobox:

```
distrobox create --image registry.fedoraproject.org/fedora-toolbox:38 --name fedora
```

To update all distrobox containers:

```
distrobox-upgrade --all
```

or

```
distrobox upgrade -a
```





