#!/usr/bin/env bash

# install flatpaks from flathub
#https://flathub.org/home
#
#flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
#  the above one installs flathub repos as a user
flatpak remote-add --user --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo


# Development
flatpak install --user --noninteractive --assumeyes flathub org.gnome.meld

# Education
flatpak install --user --noninteractive --assumeyes flathub org.kde.marble

# Graphics
flatpak install --user --noninteractive --assumeyes flathub org.blender.Blender

flatpak install --user --noninteractive --assumeyes flathub net.fasterland.converseen

flatpak install --user --noninteractive --assumeyes flathub io.gitlab.adhami3310.Converter

flatpak install --user --noninteractive --assumeyes flathub org.kde.digikam

flatpak install --user --noninteractive --assumeyes flathub org.darktable.Darktable

flatpak install --user --noninteractive --assumeyes flathub io.github.manisandro.gImageReader

flatpak install --user --noninteractive --assumeyes flathub org.gimp.GIMP

flatpak install --user --noninteractive --assumeyes flathub org.inkscape.Inkscape

flatpak install --user --noninteractive --assumeyes flathub org.kde.krita

# Internet
flatpak install --user --noninteractive --assumeyes flathub com.brave.Browser

flatpak install --user --noninteractive --assumeyes flathub org.filezillaproject.Filezilla

flatpak install --user --noninteractive --assumeyes flathub org.mozilla.firefox

flatpak install --user --noninteractive --assumeyes flathub fi.skyjake.Lagrange

flatpak install --user --noninteractive --assumeyes flathub org.getmonero.Monero

flatpak install --user --noninteractive --assumeyes flathub org.onionshare.OnionShare

flatpak install --user --noninteractive --assumeyes flathub org.qbittorrent.qBittorrent

flatpak install --user --noninteractive --assumeyes flathub com.github.micahflee.torbrowser-launcher

# Multimedia
flatpak install --user --noninteractive --assumeyes flathub org.ardour.Ardour

flatpak install --user --noninteractive --assumeyes flathub fr.handbrake.ghb

flatpak install --user --noninteractive --assumeyes flathub org.kde.kdenlive

flatpak install --user --noninteractive --assumeyes flathub com.obsproject.Studio

#flatpak install --user --noninteractive --assumeyes flathub org.openshot.OpenShot

#flatpak install --user --noninteractive --assumeyes flathub org.shotcut.Shotcut

flatpak install --user --noninteractive --assumeyes flathub org.strawberrymusicplayer.strawberry

flatpak install --user --noninteractive --assumeyes flathub org.tenacityaudio.Tenacity

flatpak install --user --noninteractive --assumeyes flathub org.videolan.VLC

# Office
flatpak install --user --noninteractive --assumeyes flathub org.libreoffice.LibreOffice

flatpak install --user --noninteractive --assumeyes flathub org.onlyoffice.desktopeditors

flatpak install --user --noninteractive --assumeyes flathub com.github.jeromerobert.pdfarranger

flatpak install --user --noninteractive --assumeyes flathub org.standardnotes.standardnotes

flatpak install --user --noninteractive --assumeyes flathub com.github.xournalpp.xournalpp

# Utilities
flatpak install --user --noninteractive --assumeyes flathub org.cryptomator.Cryptomator

flatpak install --user --noninteractive --assumeyes flathub net.sapples.LiveCaptions

flatpak install --user --noninteractive --assumeyes flathub com.nextcloud.desktopclient.nextcloud

#flatpak install --user --noninteractive --assumeyes flathub io.github.peazip.PeaZip

echo "Finished installing flatpaks for Fedora Kiniote system"




































