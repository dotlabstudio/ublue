#!/bin/bash
#
#enable cockpit services on ucore fresh systems
sudo systemctl enable --now cockpit
#
#not really needed for ucore
#Open the firewall if necessary:
sudo firewall-cmd --add-service=cockpit
sudo firewall-cmd --add-service=cockpit --permanent
#
echo "cockpit is setup now, please reboot"
