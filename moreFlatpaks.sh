#!/bin/bash

# new flatpaks and my list apps on my current sytem

#add user flathub remote 
#flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak remote-add --user --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

#Battle for Wesnoth - A turn-based strategy game with a high fantasy theme
flatpak install --user --noninteractive flathub org.wesnoth.Wesnoth

#Cider - An open source and community oriented Apple Music client
flatpak install --user --noninteractive flathub sh.cider.Cider

#Clementine Music Player - Plays music files and Internet radio - replaced by strawberry now
#flatpak install --user --noninteractive flathub org.clementine_player.Clementine

#Converseen - A batch image processor
flatpak install --user --noninteractive flathub net.fasterland.converseen

#Converter - Convert and manipulate images
flatpak install --user --noninteractive flathub io.gitlab.adhami3310.Converter

#CoreChess - Open source chess GUI for chess engines
flatpak install --user --noninteractive flathub com.github.sakya.corechess

#Cryptomator -  Multi-platform client-side encryption tool optimized for cloud storages
flatpak install --user --noninteractive flathub org.cryptomator.Cryptomator

#Czkawka - Multi functional app to find duplicates, empty folders, similar images, broken files etc.
flatpak install --user --noninteractive flathub com.github.qarmin.czkawka

#Denaro - Manage your personal finances
flatpak install --user --noninteractive flathub org.nickvision.money

#Dolphin File Manager - not needed in kde system - use the preinstall one
#flatpak install --user --noninteractive flathub org.kde.dolphin

#Endless Sky - Space exploration and combat game
flatpak install --user --noninteractive flathub io.github.endless_sky.endless_sky

#Exodus - All-in-one app to secure, manage, and exchange blockchain assets
flatpak install --user --noninteractive flathub io.exodus.Exodus

#Filelight -Show disk usage and delete unused files
flatpak install --user --noninteractive flathub org.kde.filelight

#gImageReader - A graphical (gtk) frontend to tesseract-ocr
flatpak install --user --noninteractive flathub io.github.manisandro.gImageReader

#ghostwriter - Distraction-free text editor for Markdown
flatpak install --user --noninteractive flathub io.github.wereturtle.ghostwriter

#GNOME Chess - Play the classic two-player board game of chess
flatpak install --user --noninteractive flathub org.gnome.Chess

#Joplin - A free, open source note taking and to-do application, which can handle a large number of notes organised into notebooks.
#flatpak install --user --noninteractive flathub net.cozic.joplin_desktop

#KeePassXC - Community-driven port of the Windows application “KeePass Password Safe”
flatpak install --user --noninteractive flathub org.keepassxc.KeePassXC

#Kiwix - View offline content
flatpak install --user --noninteractive flathub org.kiwix.desktop

#KNights - Chess game
flatpak install --user --noninteractive flathub org.kde.knights

#KWrite - Text Editor - kate is better but no text editor
#flatpak install --user --noninteractive flathub org.kde.kwrite

#Konversation - IRC client
#flatpak install --user --noninteractive flathub org.kde.konversation

#KStars - Desktop Planetarium
flatpak install --user --noninteractive flathub org.kde.kstars

#KTorrent - BitTorrent Client
#flatpak install --user --noninteractive flathub org.kde.ktorrent 

#Lagrange - A Beautiful Gemini Client
flatpak install --user --noninteractive flathub fi.skyjake.Lagrange

#Lokalize - Computer-aided translation system
flatpak install --user --noninteractive flathub org.kde.lokalize

#Meld - Compare and merge your files
flatpak install --user --noninteractive flathub org.gnome.meld

#merkato - Track of your investments
flatpak install --user --noninteractive flathub com.ekonomikas.merkato

#MusicBrainz Picard - MusicBrainz's music tagger
flatpak install --user --noninteractive flathub org.musicbrainz.Picard

#OnionShare - Securely and anonymously share files, host websites, and chat with friends
flatpak install --user --noninteractive flathub org.onionshare.OnionShare

#Photopea - Free alternative for Adobe Photoshop users
flatpak install --user --noninteractive flathub com.github.vikdevelop.photopea_app

#qBittorrent - An open-source Bittorrent client
flatpak install --user --noninteractive flathub org.qbittorrent.qBittorrent

#Qmmp - Qt-based Multimedia Player
#flatpak install --user --noninteractive flathub com.ylsoftware.qmmp.Qmmp

#QMPlay2 - Video and audio player
flatpak install --user --noninteractive flathub io.github.zaps166.QMPlay2

#SMPlayer - A great media player
flatpak install --user --noninteractive flathub info.smplayer.SMPlayer

#Sublime Text - Sophisticated text editor for code, markup and prose
flatpak install --user --noninteractive flathub com.sublimetext.three

#Tube Converter - An easy-to-use YouTube video downloader
flatpak install --user --noninteractive flathub org.nickvision.tubeconverter

#Warp - Fast and secure file transfer - based on wormhole
flatpak install --user --noninteractive flathub app.drey.Warp

#Warpinator - Send and Receive Files across the Network
flatpak install --user --noninteractive flathub org.x.Warpinator

#Wireshark - Wireshark is the world's foremost protocol analyzer
flatpak install --user --noninteractive flathub org.wireshark.Wireshark

#Zettlr - A Markdown editor for the 21st century
flatpak install --user --noninteractive flathub com.zettlr.Zettlr

echo "Finished installing my list of apps as flatpaks"
