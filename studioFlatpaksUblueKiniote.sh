#!/bin/bash
# ubuntu studio, fedora design suite, av mx edition and nobara apps as flatpaks
#
#add flathub user remote 
#flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak remote-add --user --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

#audio production

flatpak install --user --noninteractive flathub org.ardour.Ardour

flatpak install --user --noninteractive flathub ca.littlesvr.asunder

flatpak install --user --noninteractive flathub org.audacityteam.Audacity

flatpak install --user --noninteractive flathub studio.kx.carla

flatpak install --user --noninteractive flathub org.gnome.EasyTAG

flatpak install --user --noninteractive flathub org.guitarix.Guitarix

flatpak install --user --noninteractive flathub fr.handbrake.ghb

flatpak install --user --noninteractive flathub org.hydrogenmusic.Hydrogen

flatpak install --user --noninteractive flathub net.mediaarea.MediaInfo

flatpak install --user --noninteractive flathub org.kde.kid3

flatpak install --user --noninteractive flathub io.lmms.LMMS

flatpak install --user --noninteractive flathub org.musescore.MuseScore

flatpak install --user --noninteractive flathub com.polyphone_soundfonts.polyphone

flatpak install --user --noninteractive flathub org.rncbc.qpwgraph

flatpak install --user --noninteractive flathub org.rncbc.qsynth

flatpak install --user --noninteractive flathub org.rncbc.qtractor

#graphics design

#flatpak install --user --noninteractive flathub nl.hjdskes.gcolor3

flatpak install --user --noninteractive flathub org.darktable.Darktable

flatpak install --user --noninteractive flathub org.kde.digikam

#flatpak install --user --noninteractive flathub org.entangle_photo.Manager
#used old gnome runtime

flatpak install --user --noninteractive flathub org.gimp.GIMP

#flatpak install --user --noninteractive flathub org.kde.gwenview

flatpak install --user --noninteractive flathub net.sourceforge.Hugin

flatpak install --user --noninteractive flathub org.inkscape.Inkscape

flatpak install --user --noninteractive flathub org.kde.kcolorchooser

flatpak install --user --noninteractive flathub org.kde.krita

#flatpak install --user --noninteractive flathub org.mypaint.MyPaint
#used old gnome runtime

#flatpak install --user --noninteractive flathub org.kde.okular

#flatpak install --user --noninteractive flathub org.gnome.Photos

#flatpak install --user --noninteractive flathub org.gnome.Shotwell
#used old gnome runtime - digikam is much better

flatpak install --user --noninteractive flathub com.rawtherapee.RawTherapee

#video production

flatpak install --user --noninteractive flathub org.avidemux.Avidemux

flatpak install --user --noninteractive flathub org.blender.Blender

flatpak install --user --noninteractive flathub org.kde.kdenlive

#flatpak install --user --noninteractive flathub io.github.jliljebl.Flowblade
#used old gnome runtime

flatpak install --user --noninteractive flathub com.obsproject.Studio

flatpak install --user --noninteractive flathub org.olivevideoeditor.Olive

flatpak install --user --noninteractive flathub org.openshot.OpenShot

#flatpak install --user --noninteractive flathub org.pitivi.Pitivi
#used old gnome runtime

flatpak install --user --noninteractive flathub org.shotcut.Shotcut

flatpak install --user --noninteractive flathub com.ozmartians.VidCutter

#flatpak install --user --noninteractive flathub org.gnome.Totem

#internet

#flatpak install --user --noninteractive flathub org.mozilla.firefox

#flatpak install --user --noninteractive flathub org.quassel_irc.QuasselClient

#flatpak install --user --noninteractive flathub org.mozilla.Thunderbird

#media playback
flatpak install --user --noninteractive flathub org.kde.elisa

#flatpak install --user --noninteractive flathub org.gnome.Rhythmbox3

flatpak install --user --noninteractive flathub io.mpv.Mpv

flatpak install --user --noninteractive flathub org.videolan.VLC

flatpak install --user --noninteractive flathub com.github.vkohaupt.vokoscreenNG

#office

flatpak install --user --noninteractive flathub org.fontforge.FontForge

flatpak install --user --noninteractive flathub org.libreoffice.LibreOffice

flatpak install --user --noninteractive flathub org.onlyoffice.desktopeditors 

flatpak install --user --noninteractive flathub com.github.jeromerobert.pdfarranger

flatpak install --user --noninteractive flathub com.github.xournalpp.xournalpp

#utilities 

#flatpak install --user --noninteractive flathub org.kde.ark

#flatpak install --user --noninteractive flathub org.gnome.Cheese

flatpak install --user --noninteractive flathub org.kde.kamoso

flatpak install --user --noninteractive flathub org.fcitx.Fcitx5

#flatpak install --user --noninteractive flathub org.kde.kcalc

#flatpak install --user --noninteractive flathub org.kde.kfind

flatpak install --user --noninteractive flathub org.ksnip.ksnip

flatpak install --user --noninteractive flathub org.openrgb.OpenRGB

flatpak install --user --noninteractive flathub org.gnome.SimpleScan

#pin to the taskbar
#firefox ardour obs-studio krita kdenlive digikam darktable
#

#gaming stuff

flatpak install --user --noninteractive flathub net.lutris.Lutris

flatpak install --user --noninteractive flathub net.davidotek.pupgui2

flatpak install --user --noninteractive flathub com.valvesoftware.Steam

echo "Finished installing studio flatpaks for ublue kiniote"
