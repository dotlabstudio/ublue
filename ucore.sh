#!/bin/bash
#
#post install script for a fresh ucore server system
sudo rpm-ostree install cockpit-kdump cockpit-machines cockpit-ostree cockpit-pcp cockpit-sosreport fish htop neofetch util-linux-user yt-dlp
#
#enable tailscale and cockpit services
#
#Use systemctl to enable and start the service:
sudo systemctl enable --now tailscaled
#
#Enable cockpit:
sudo systemctl enable --now cockpit
#Open the firewall if necessary:
sudo firewall-cmd --add-service=cockpit
sudo firewall-cmd --add-service=cockpit --permanent
#
echo "please reboot the system"
